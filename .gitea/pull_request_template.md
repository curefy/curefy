---

name: "Default"
about: "Default pull request template"
title: "PR/master: "
ref: "master"
labels:

---


### Description

### Related issue

### Motivation and Context

### How has this been tested?

### Screenshots (if appropriate)

### Type of change

[ ] Bug fix or patch (non-breaking fix of an issue: v_._.X)
[ ] New feature (non-breaking adding functionality: v_.X._)
[ ] Breaking change (fix or feature that breaks existing functionality: vX._._)

### Have you read our CODE_OF_CONDUCT.md, CONTRIBUTING.md, and other documents?

[ ] yes
[ ] no
