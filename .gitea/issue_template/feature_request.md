---

name: "Feature Request"
about: "Suggest a feature to be implemented."
title: "[FR]/master "
ref: "master"
labels:

- enhancement
- "help wanted"

---

### Detailed Description

**\<describe what you want the feature to do>**

### Possible Implementation

**\<describe how you would implement the feature>**

### Environment (answer all that apply)

Operating System (OS) name and version: 

libc name and version: 

Browser name and version: 

Version of each dependency: 

**\<list any relevant environment variables, software, or other context here>**

### If FR is approved, are you willing to submit a PR and contribute?

[ ] yes
[ ] no
[ ] maybe
[ ] what's a PR?
